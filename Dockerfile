# Docker-gitbackup, by Isaac Towns <Zelec@timeguard.ca>

# Rather than making the git repo a submodule of this repo, it would be better off to change the docker file as needed.
FROM alpine/git:1.0.7 AS repo
# Change target release tag here

# Normally alpine/git uses /git for it's workdir, however it's declared as a volume in the upstream resource
# so it has a hard time keeping data intact inbetween layers.
RUN mkdir /app
WORKDIR /app
RUN git clone https://github.com/amitsaha/gitbackup.git . && \
    git checkout `git describe --abbrev=0 --tags --match "[0-9]*" $(git rev-list --tags --max-count=1)`

# Go builder, don't need go in final container to run app.
FROM library/golang:1.13.8-alpine3.11 AS builder
COPY --from=repo /app /app
WORKDIR /app
RUN go build

FROM library/alpine:3.11 as docker-gitbackup
LABEL maintainer Isaac Towns <Zelec@timeguard.ca>
# Pull built go app
COPY --from=builder /app/gitbackup /bin/
COPY /root/ /
RUN \
    mkdir -p /defaults /config && \
    apk --no-cache --update-cache add dumb-init git openssh git-lfs bash shadow && \
    useradd -u 911 -U -d /config -s /bin/false user && \
    usermod -G users user
VOLUME ["/data", "/config"]
ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/entrypoint.sh"]