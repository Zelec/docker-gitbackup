#!/usr/bin/dumb-init /bin/bash

# Ripped from Linuxserver.io baseimages.
PUID=${PUID:-911}
PGID=${PGID:-911}

groupmod -o -g "$PGID" user
usermod -o -u "$PUID" user
chown -R user:user /config
chown -R user:user /defaults
chown -R user:user /data
su -s /bin/bash user -c "git lfs install --skip-repo"

if [ "$(ls -A '/config/.ssh')" ]; then
    echo "Volume has files, using existing config..."
else
    echo "Volume Empty, copying base config files into volume..."
    cp -rT /defaults /config
    chown -R user:user /config
    echo "...Done"
fi
if [ -z "$GITLAB_TOKEN" ] && [ -z "$GITHUB_TOKEN" ]; then
    echo "No Github/Gitlab token spesified, exiting..."
    exit 1
fi

BACKUP_DIR=${BACKUP_DIR:-/data}

COMMANDLINE="/bin/gitbackup"
if [ -n "$BACKUP_DIR" ]; then
    COMMANDLINE="$COMMANDLINE -backupdir $BACKUP_DIR"
fi
if [ -n "$GITHOST_URL" ]; then
    COMMANDLINE="$COMMANDLINE -githost.url $GITHOST_URL"
fi
if [ -n "$GITHUB_REPOTYPE" ]; then
    COMMANDLINE="$COMMANDLINE -github.repoType $GITHUB_REPOTYPE"
fi
if [ -n "$GITLAB_PROJECT_MEMBERSHIPTYPE" ]; then
    COMMANDLINE="$COMMANDLINE -gitlab.projectMembershipType $GITLAB_PROJECT_MEMBERSHIPTYPE"
fi
if [ -n "$GITLAB_PROJECT_VISIBILITY" ]; then
    COMMANDLINE="$COMMANDLINE -gitlab.projectVisibility $GITLAB_PROJECT_VISIBILITY"
fi
if [ -n "$IGNORE_PRIVATE" ]; then
    COMMANDLINE="$COMMANDLINE -ignore-private"
fi
if [ -n "$SERVICE" ]; then
    COMMANDLINE="$COMMANDLINE -service $SERVICE"
fi
if [ -n "$HTTPS_CLONE" ]; then
    COMMANDLINE="$COMMANDLINE -use-https-clone"
fi

find /config -type d -exec chmod 700 {} \;
find /config -type f -exec chmod 600 {} \;
su -s /bin/bash user -c "$COMMANDLINE"


