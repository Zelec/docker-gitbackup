# Docker GitBackup

This is a Docker Image for running [Amitsaha's](https://github.com/amitsaha) [GitBackup](https://github.com/amitsaha/gitbackup) software

From gitbackup's [README.md](https://github.com/amitsaha/gitbackup/blob/master/README.md) file:

    gitbackup is a tool to backup your git repositories from GitHub (including GitHub enterprise) or GitLab (including custom GitLab installations).

    gitbackup only creates a backup of the repository and does not currently support issues, pull requests or other data associated with a git repository.

To understand all available options & features, follow along with gitbackup's [README.md](https://github.com/amitsaha/gitbackup/blob/master/README.md) file and review the [template.env](./template.env) file in this repo

## Quickstart

1. Make a `gitbackup` folder, cd into it, and make a `docker-compose.yml` file:
``` yaml
version: '2'
services:
    git-mirror:
        image: zelec/gitbackup:latest
        env_file: 
            - .env
        volumes:
            - ./data:/data
            - ./config:/config
```
2. Copy the template.env file from the [repo](https://gitlab.com/Zelec/Docker-Gitbackup) and put it at `.env`
3. Configure the `.env` to your liking with a Github or Gitlab token
4. The backup program only pulls a list and auto calls `git clone`, So for actually pulling your code to backup there are 3 options:
    - create a custom ssh key with `ssh-keygen -C gitbackup -f ./config/.ssh/id_rsa` and authorizing it for your Gitlab/Github account
    - Put your existing keys into `./config/.ssh`
    - use the `$HTTPS_CLONE` environment variable (Fair warning, your token and username will be placed in the .git/config file of each repo)

Now you can use `docker-compose run --rm git-mirror` and backup your git repositories to the `./data` folder

This container is manual only, but it's simple enough to setup cron on the host machine to run this as needed.

for example:
```
SHELL=/bin/bash
0 * * * * docker run --rm -v /home/user/gitbackup/data:/data -v /home/user/gitbackup/config:/config --env-file /home/user/gitbackup/.env zelec/gitbackup:latest
```

The above cron snippet will autorun the container each hour with the defined environment variables in `.env`